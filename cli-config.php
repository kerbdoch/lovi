<?php

require_once "vendor/autoload.php";

$app = new \Kerbdoch\Lovi\Application();
$app->run(\Kerbdoch\Lovi\Application::CONTEXT_CLI);

return \Doctrine\ORM\Tools\Console\ConsoleRunner::createHelperSet($app::getContainer()->get('em'));
