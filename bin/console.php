<?php
require_once __DIR__ . "/../vendor/autoload.php";

call_user_func(function () {
    $app = new Kerbdoch\Lovi\Application();
    $app->run(Kerbdoch\Lovi\Application::CONTEXT_CLI);
});
