<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Test\Unit\Logging;

use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Logging\LoggerFactory;
use Monolog\Handler\NullHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

class LoggerFactoryTest extends TestCase
{
    public function testBuildLogger()
    {
        $logger = LoggerFactory::buildLogger("test1");
        $this->assertInstanceOf(Logger::class, $logger);
        $this->assertSame("test1", $logger->getName());
    }

    public function testBuildHandlerWithoutArgumentArrayWorksAsExpected()
    {
        $handler = LoggerFactory::buildHandler(NullHandler::class);

        $this->assertInstanceOf(NullHandler::class, $handler);
    }

    public function testBuildHandlerWithoutRequiredArgumentsThrowsException()
    {
        $this->expectException(ApplicationException::class);
        LoggerFactory::buildHandler(StreamHandler::class, []);
    }

    public function testBuildHandlerTakesNamedArguments()
    {
        $arguments = [
            "bubble" => false,
            "stream" => "php://memory"
        ];
        $this->assertInstanceOf(
            StreamHandler::class,
            LoggerFactory::buildHandler(StreamHandler::class, $arguments)
        );
    }
}
