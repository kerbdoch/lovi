<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Test\Unit\Reflection;

use Kerbdoch\Lovi\Reflection\ObjectFactory;
use Kerbdoch\Lovi\Reflection\ReflectionException;
use Kerbdoch\Lovi\Test\Helper\ClassWithRequiredConstructorArgument;
use PHPUnit\Framework\TestCase;

class ObjectFactoryTest extends TestCase
{
    /**
     * @test
     */
    public function newThrowsReflectionExceptionIfClassDoesNotExist()
    {
        $this->expectException(ReflectionException::class);

        ObjectFactory::new('Invalid\\Class\\This\\Does\\Not\\Exist');
    }

    /**
     * @test
     */
    public function newThrowsReflectionExceptionIfRequiredConstructorArgumentIsMissing()
    {
        $this->expectException(ReflectionException::class);

        ObjectFactory::new(ClassWithRequiredConstructorArgument::class);
    }

    /**
     * @test
     */
    public function newReturnsNewInstanceOfTheGivenClass()
    {
        $object = ObjectFactory::new(ClassWithRequiredConstructorArgument::class, ['required' => true]);

        $this->assertInstanceOf(
            ClassWithRequiredConstructorArgument::class,
            $object
        );
    }
}
