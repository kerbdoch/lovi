<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Test\Unit\Cache;

use Kerbdoch\Lovi\Cache\CacheFactory;
use Kerbdoch\Lovi\Exception\CacheDoesNotExistException;
use PHPUnit\Framework\TestCase;
use Psr\Cache\CacheItemPoolInterface;

class CacheFactoryTest extends TestCase
{
    /**
     * @var CacheFactory
     */
    protected $subject;

    protected function setUp()
    {
        $this->subject = new CacheFactory();
    }

    protected function tearDown()
    {
        if ($this->subject) {
            $this->subject->reset();
        }
    }

    /**
     * @test
     */
    public function existsReturnsTrueForAnExistingCacheOnFirstCallAndSubsequentCalls()
    {
        $this->assertTrue(
            $this->subject->exists('cacheOne'),
            'exists returns true on first call'
        );

        $this->assertTrue(
            $this->subject->exists('cacheOne'),
            'exists returns true on second call'
        );
    }

    /**
     * @test
     */
    public function existsReturnsFalseForUnknownCaches()
    {
        $this->assertFalse(
            $this->subject->exists('doesNotExist')
        );
    }

    /**
     * @test
     */
    public function getThrowsCacheDoesNotExistExceptionForUnknownCaches()
    {
        $this->expectException(CacheDoesNotExistException::class);

        $this->subject->get('doesNotExist');
    }

    /**
     * @test
     */
    public function getReturnsCacheItemPoolInterface()
    {
        $this->assertInstanceOf(
            CacheItemPoolInterface::class,
            $this->subject->get('cacheOne')
        );
    }

    /**
     * @test
     */
    public function allReturnsGeneratorForAllCaches()
    {
        $cacheCount = 0;

        foreach ($this->subject->all() as [$name, $cache]) {
            $this->assertInstanceOf(
                CacheItemPoolInterface::class,
                $cache
            );
            $cacheCount++;
        }

        $this->assertSame(2, $cacheCount);
    }

    /**
     * @test
     */
    public function listReturnsListOfAllRegisteredCaches()
    {
        $list = $this->subject->list();

        $this->assertCount(2, $list);
        $this->assertArraySubset(['cacheOne', 'cacheTwo'], $list);
    }
}
