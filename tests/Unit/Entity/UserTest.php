<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Test\Unit\Entity;

use Kerbdoch\Lovi\Entity\User;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    /**
     * @var User
     */
    protected $subject;

    protected function setUp()
    {
        $this->subject = new User();
    }

    public function testUsernameIsAlwaysLowercase()
    {
        $username = "lOrEmIpSuM";
        $this->subject->setUsername($username);

        $this->assertSame(
            "loremipsum",
            $this->subject->getUsername()
        );
    }
}
