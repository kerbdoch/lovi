<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Annotations;

class AnnotationLoader
{
    public static function load(string $class)
    {
        if (substr($class, 0, 25) === 'Kerbdoch\Lovi\Annotations'
            && class_exists($class)
        ) {
            return true;
        }

        return null;
    }
}
