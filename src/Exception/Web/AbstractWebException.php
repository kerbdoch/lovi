<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Exception\Web;

use Kerbdoch\Lovi\Exception\ApplicationException;

abstract class AbstractWebException extends ApplicationException
{
    abstract public function getStatusCode(): int;

    abstract public function getStatusMessage(): string;

    /**
     * @return array
     */
    public function getStatus(): array
    {
        return [$this->getStatusCode(), $this->getStatusMessage()];
    }
}
