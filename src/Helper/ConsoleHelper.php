<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Helper;

use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Utility\PathUtility;
use Symfony\Component\Console\Command\Command;

class ConsoleHelper
{
    public function findCommands()
    {
        $files = glob(PathUtility::normalizePath(__DIR__ . "/../Command/") . "*Command.php");
        foreach ($files as $file) {
            $className = substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1);
            $className = substr($className, 0, strlen($className) - 4);
            $className = "Kerbdoch\\Lovi\\Command\\" . $className;

            if (class_exists($className)) {
                if (!is_subclass_of($className, Command::class)) {
                    throw new ApplicationException(
                        "Command $className does not extend " . Command::class,
                        1494498148
                    );
                }
                yield new $className();
            }
        }
    }
}
