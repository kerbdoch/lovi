<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Helper;

use Composer\Script\Event;
use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Cache\CacheFactory;
use Psr\Cache\CacheItemPoolInterface;

class ComposerHelper
{
    public static function postAutoloadDump(Event $event)
    {
        $vendorDir = $event->getComposer()->getConfig()->get('vendor-dir');
        require $vendorDir . '/autoload.php';

        $app = new Application();
        $app->run(Application::CONTEXT_BOOTSTRAP_ONLY);

        static::clearAllCaches();
    }

    private static function clearAllCaches()
    {
        $cacheFactory = new CacheFactory();

        /**
         * @var string $name
         * @var CacheItemPoolInterface $cache
         */
        foreach ($cacheFactory->all() as [$name, $cache]) {
            $cache->clear();
        }

        echo "Cleared all available caches.\n";
    }
}
