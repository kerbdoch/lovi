<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Entity;

/**
 * Class Notch
 *
 * @Entity
 * @Table(name="notches")
 */
class Notch extends AbstractEntity
{
    /**
     * @Column(type="datetime")
     *
     * @var \DateTime
     */
    protected $timestamp;

    /**
     * @ManyToOne(targetEntity="Tally")
     * @JoinColumn(name="tally_id", referencedColumnName="id")
     *
     * @var Tally
     */
    protected $tally;

    /**
     * Notch constructor.
     */
    public function __construct()
    {
        $this->timestamp = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getTimestamp(): \DateTime
    {
        return $this->timestamp;
    }
}
