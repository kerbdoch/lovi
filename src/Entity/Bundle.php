<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Bundle
 *
 * @Entity
 * @Table(name="bundles")
 */
class Bundle extends AbstractEntity
{
    /**
     * @Column(type="string", nullable=false)
     *
     * @var string
     */
    protected $name;

    /**
     * @Column(type="text")
     *
     * @var string
     */
    protected $description;

    /**
     * @OneToMany(targetEntity="Tally", mappedBy="bundle")
     *
     * @var ArrayCollection|Tally[]
     */
    protected $tallies;

    /**
     * @OneToMany(targetEntity="Membership", mappedBy="bundle")
     *
     * @var ArrayCollection|Membership[]
     */
    protected $memberships;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): Bundle
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return ArrayCollection|Tally[]
     */
    public function getTallies(): ArrayCollection
    {
        return $this->tallies;
    }

    /**
     * @param ArrayCollection $tallies
     *
     * @return self
     */
    public function setTallies(ArrayCollection $tallies)
    {
        $this->tallies = $tallies;

        return $this;
    }

    /**
     * @param Tally $tally
     *
     * @return self
     */
    public function addTally(Tally $tally): Bundle
    {
        $this->tallies->add($tally);

        return $this;
    }

    /**
     * @param Tally $tally
     *
     * @return self
     */
    public function removeTally(Tally $tally): Bundle
    {
        $this->tallies->remove($tally);

        return $this;
    }

    /**
     * @return ArrayCollection|Membership[]
     */
    public function getMemberships()
    {
        return $this->memberships;
    }

    /**
     * @param ArrayCollection|Membership[] $memberships
     * @return self
     */
    public function setMemberships($memberships): Bundle
    {
        $this->memberships = $memberships;
        return $this;
    }

    /**
     * @param Membership $membership
     * @return self
     */
    public function addMembership(Membership $membership): Bundle
    {
        $this->memberships->add($membership);
        return $this;
    }

    /**
     * @param Membership $membership
     * @return self
     */
    public function removeMembership(Membership $membership): Bundle
    {
        $this->memberships->remove($membership);
        return $this;
    }
}
