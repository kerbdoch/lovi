<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Entity;

/**
 * Class Membership
 *
 * @Entity
 * @Table(name="memberships")
 */
class Membership extends AbstractEntity
{
    /**
     * @ManyToOne(targetEntity="Bundle")
     * @JoinColumn(name="bundle_id", referencedColumnName="id")
     *
     * @var Bundle
     */
    protected $bundle;

    /**
     * @ManyToOne(targetEntity="User")
     * @JoinColumn(name="user_id", referencedColumnName="id")
     *
     * @var User
     */
    protected $user;

    /**
     * @Column(type="boolean")
     *
     * @var bool
     */
    protected $superuser = false;

    /**
     * @return Bundle
     */
    public function getBundle(): Bundle
    {
        return $this->bundle;
    }

    /**
     * @param Bundle $bundle
     * @return self
     */
    public function setBundle(Bundle $bundle): Membership
    {
        $this->bundle = $bundle;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return self
     */
    public function setUser(User $user): Membership
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return bool
     */
    public function isSuperuser(): bool
    {
        return $this->superuser;
    }

    /**
     * @param bool $superuser
     * @return self
     */
    public function setSuperuser(bool $superuser): Membership
    {
        $this->superuser = $superuser;
        return $this;
    }
}
