<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Tally
 *
 * @Entity
 * @Table(name="tallies")
 */
class Tally extends AbstractEntity
{
    /**
     * @Column(type="string")
     *
     * @var string
     */
    protected $name;

    /**
     * @ManyToOne(targetEntity="Bundle")
     * @JoinColumn(name="bundle_id", referencedColumnName="id")
     *
     * @var Bundle
     */
    protected $bundle;

    /**
     * @OneToMany(targetEntity="Notch", mappedBy="tally")
     *
     * @var ArrayCollection|Notch[]
     */
    protected $notches;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Bundle
     */
    public function getBundle(): Bundle
    {
        return $this->bundle;
    }

    /**
     * @param Bundle $bundle
     *
     * @return self
     */
    public function setBundle(Bundle $bundle)
    {
        $this->bundle = $bundle;

        return $this;
    }

    /**
     * @return ArrayCollection|Notch[]
     */
    public function getNotches(): ArrayCollection
    {
        return $this->notches;
    }

    /**
     * @param ArrayCollection $notches
     * @return self
     */
    public function setNotches(ArrayCollection $notches): Tally
    {
        $this->notches = $notches;

        return $this;
    }
}
