<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Entity;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class User
 *
 * @Entity
 * @Table(name="users")
 */
class User extends AbstractEntity
{
    /**
     * @Column(type="string")
     *
     * @var string
     */
    protected $username;

    /**
     * @Column(type="string")
     *
     * @var string
     */
    protected $digest;

    /**
     * @Column(type="string")
     *
     * @var string
     */
    protected $email;

    /**
     * @Column(type="string")
     *
     * @var string
     */
    protected $timezone;

    /**
     * @OneToMany(targetEntity="Membership", mappedBy="user")
     *
     * @var ArrayCollection|Membership[]
     */
    protected $memberships;

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return self
     */
    public function setUsername(string $username): User
    {
        $this->username = mb_strtolower($username);
        return $this;
    }

    /**
     * @return string
     */
    public function getDigest(): string
    {
        return $this->digest;
    }

    /**
     * @param string $digest
     * @return self
     */
    public function setDigest(string $digest): User
    {
        $this->digest = $digest;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): User
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getTimezone(): string
    {
        return $this->timezone;
    }

    /**
     * @param string $timezone
     * @return self
     */
    public function setTimezone(string $timezone): User
    {
        $this->timezone = $timezone;
        return $this;
    }
}
