<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Security;

use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Security\Rule\RuleInterface;
use Psr\Http\Message\ServerRequestInterface;
use Kerbdoch\Lovi\Annotations;

class AccessControl
{
    /**
     * @var Annotations\AccessControl
     */
    protected $accessControlAnnotation;

    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * AccessControl constructor.
     * @param Annotations\AccessControl $accessControlAnnotation
     * @param ServerRequestInterface $request
     */
    public function __construct(
        Annotations\AccessControl $accessControlAnnotation,
        ServerRequestInterface $request
    ) {
        $this->accessControlAnnotation = $accessControlAnnotation;
        $this->request = $request;
    }

    public function userHasAccess(): bool
    {
        /** @var Authentication $userAuth */
        $userAuth = Application::getContainer()->get('auth');
        foreach ($this->accessControlAnnotation->value as $rule) {
            $rule = $this->getRule($rule);
            if (!$rule->checkAccess($userAuth, $this->request)) {
                return false;
            }
        }

        return true;
    }

    protected function getRule(string $rule): RuleInterface
    {
        if (strpos('\\', $rule) === false) {
            $className = 'Kerbdoch\\Lovi\\Security\\Rule\\' . $rule;
        } else {
            $className = $rule;
        }

        if (!class_exists($className)) {
            throw new ApplicationException(
                "The access rule \"$className\" could not be found.",
                1495205312603
            );
        } elseif (!in_array(RuleInterface::class, class_implements($className))) {
            throw new ApplicationException(
                "The access rule \"$className\" does not implement RuleInterface.",
                1495205348509
            );
        }

        return new $className;
    }
}
