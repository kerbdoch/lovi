<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Security\Rule;

use Doctrine\ORM\EntityManager;
use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Entity\Membership;
use Kerbdoch\Lovi\Security\Authentication;
use Psr\Http\Message\ServerRequestInterface;

class BundleOwnership implements RuleInterface
{
    public function checkAccess(Authentication $authentication, ServerRequestInterface $request): bool
    {
        if (is_null($authentication->getUser())) {
            return false;
        }

        /** @var EntityManager $em */
        $em = Application::getContainer()->get('em');
        $repo = $em->getRepository(Membership::class);

        $membership = $repo->findOneBy(
            [
                'bundle'    => $request->getAttribute('bundle'),
                'user'      => $authentication->getUser(),
                'superuser' => true
            ]
        );

        return !is_null($membership);
    }
}
