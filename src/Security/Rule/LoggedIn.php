<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Security\Rule;

use Kerbdoch\Lovi\Security\Authentication;
use Psr\Http\Message\ServerRequestInterface;

class LoggedIn implements RuleInterface
{
    public function checkAccess(Authentication $authentication, ServerRequestInterface $request): bool
    {
        return $authentication->isValid();
    }
}
