<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Cache;

use Cache\Bridge\Doctrine\DoctrineCacheBridge;
use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Exception\CacheDoesNotExistException;
use Kerbdoch\Lovi\Reflection\ObjectFactory;
use Psr\Cache\CacheItemPoolInterface;

class CacheFactory
{
    /**
     * @var CacheItemPoolInterface[]
     */
    protected static $caches = [];

    public function exists($cacheName): bool
    {
        if (array_key_exists($cacheName, static::$caches)) {
            return true;
        }

        $config = Application::getContainer()->get('config')['cache'];
        return array_key_exists($cacheName, $config);
    }

    public function get($cacheName): CacheItemPoolInterface
    {
        if (!$this->exists($cacheName)) {
            throw new CacheDoesNotExistException(
                "The requested cache \"$cacheName\" is not registered.",
                1495027328487
            );
        }

        return static::$caches[$cacheName] ?? $this->buildCache($cacheName);
    }

    public function getDoctrineCacheBridge($cacheName): DoctrineCacheBridge
    {
        return new DoctrineCacheBridge(
            $this->get($cacheName)
        );
    }

    /**
     * @return string[]
     */
    public function list(): array
    {
        $config = Application::getContainer()->get('config')['cache'];

        return array_keys($config);
    }

    /**
     * @return \Generator
     */
    public function all()
    {
        $this->buildAllCaches();

        foreach (static::$caches as $name => $cache) {
            yield [$name, $cache];
        }
    }

    public function reset()
    {
        $caches = array_keys(static::$caches);

        foreach ($caches as $cache) {
            static::$caches[$cache]->commit();
            unset(static::$caches[$cache]);
        }

        static::$caches = [];
    }

    protected function buildCache($cacheName): CacheItemPoolInterface
    {
        $cacheConfig = Application::getContainer()->get('config')['cache'][$cacheName];

        if (!class_exists($cacheConfig['adapter'])
            || !in_array(CacheItemPoolInterface::class, class_implements($cacheConfig['adapter']))
        ) {
            throw new ApplicationException(
                "The cache adapter class \"{$cacheConfig['adapter']}\" does not exist or does not implement Psr\\Cache\\CacheItemPoolInterface.",
                1495027651630
            );
        }

        $cache = ObjectFactory::new($cacheConfig['adapter'], $cacheConfig['config']);
        static::$caches[$cacheName] = $cache;

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $cache;
    }

    protected function buildAllCaches()
    {
        $cacheNames = array_keys(Application::getContainer()->get('config')['cache']);

        foreach ($cacheNames as $cacheName) {
            if (array_key_exists($cacheName, static::$caches)) {
                continue;
            }
            $this->buildCache($cacheName);
        }
    }
}
