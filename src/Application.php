<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi;

use Cache\Bridge\Doctrine\DoctrineCacheBridge;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Tools\Console\Helper\ConnectionHelper;
use Doctrine\ORM;
use Kerbdoch\Lovi\Annotations\AnnotationLoader;
use Kerbdoch\Lovi\Cache\CacheFactory;
use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Logging\LoggerFactory;
use Kerbdoch\Lovi\Helper\ConsoleHelper;
use Kerbdoch\Lovi\Helper\RouteHelper;
use Kerbdoch\Lovi\Utility\PathUtility;
use Kerbdoch\Lovi\Web\RequestHandler;
use League\Container\Container;
use League\Route\RouteCollection;
use League\Route\Strategy\JsonStrategy;
use Noodlehaus\Config;
use Symfony\Component\Cache\CacheItem;
use Symfony\Component\Console\Helper\HelperSet;

/**
 * Class Application
 *
 * @SWG\Info(title="Lovi API", version="0.1")
 */
class Application
{
    const VERSION = '0.0.1';

    /**
     * @var Container
     */
    protected static $container;

    /**
     * @var bool
     */
    protected static $inDevMode;

    /**
     * @var CacheFactory
     */
    protected $cacheFactory;

    const CONTEXT_WEB = 'web';
    const CONTEXT_CLI = 'cli';
    const CONTEXT_TESTING = 'test';
    const CONTEXT_BOOTSTRAP_ONLY = 'bootstrap';

    public function run($context)
    {
        switch ($context) {
            case static::CONTEXT_WEB:
                chdir(PathUtility::normalizePath(__DIR__ . '/../web'));
                $this->bootstrap();
                $this->handleWebRequest();
                break;
            case static::CONTEXT_CLI:
                chdir(PathUtility::normalizePath(__DIR__ . '/../bin'));
                $this->bootstrap();
                $this->handleCliRequest();
                break;
            case static::CONTEXT_TESTING:
                chdir(PathUtility::normalizePath(__DIR__ . '/../tests'));
                $this->bootstrapTestingEnvironment();
                break;
            case static::CONTEXT_BOOTSTRAP_ONLY:
                $this->bootstrap();
                break;
            default:
                throw new ApplicationException(
                    "Unknown context '$context'.",
                    1494496331
                );
        }
    }

    protected function bootstrap()
    {
        mb_internal_encoding("UTF-8");
        static::$container = new Container();
        static::$container->share('config', function () {
            return Config::load([
                PathUtility::normalizePath(__DIR__ . '/../config/config.dist.json'),
                PathUtility::normalizePath(__DIR__ . '/../config/config.json')
            ]);
        });

        $this->registerAnnotations();
        $this->registerLoggers();
        $this->registerDatabaseConnection();
        $this->registerEntityManager();

        static::$inDevMode = boolval(static::$container->get('config')['devMode']);
    }

    protected function bootstrapTestingEnvironment()
    {
        mb_internal_encoding("UTF-8");
        static::$container = new Container();
        static::$container->share('config', function () {
            return Config::load([
                PathUtility::normalizePath(__DIR__ . '/../tests/config.json')
            ]);
        });

        $this->registerLoggers();
        $this->registerDatabaseConnection();
        $this->registerEntityManager();
    }

    protected function handleWebRequest()
    {
        static::$container->share('request', \GuzzleHttp\Psr7\ServerRequest::fromGlobals());
        static::$container->share('response', \GuzzleHttp\Psr7\Response::class);

        $handler = new RequestHandler();
        $handler->handle();
        $handler->output();
    }

    protected function handleCliRequest()
    {
        $consoleHelper = new ConsoleHelper();

        $cli = new \Symfony\Component\Console\Application('Lovi', static::VERSION);
        $doctrineHelperSet = new HelperSet([
            'db' => new ConnectionHelper(static::$container->get('db')),
            'em' => new ORM\Tools\Console\Helper\EntityManagerHelper(static::$container->get('em'))
        ]);
        $cli->setHelperSet($doctrineHelperSet);
        ORM\Tools\Console\ConsoleRunner::addCommands($cli);

        foreach ($consoleHelper->findCommands() as $command) {
            $cli->add($command);
        }

        $cli->run();
    }

    protected function registerAnnotations()
    {
        AnnotationRegistry::registerLoader([AnnotationLoader::class, 'load']);

        static::$container->share('annotation_reader', function () {
            $cacheFactory = new CacheFactory();

            return new CachedReader(
                new AnnotationReader(),
                $cacheFactory->getDoctrineCacheBridge('reflection'),
                static::$inDevMode
            );
        });
    }

    protected function registerLoggers()
    {
        static::$container->share('log', function () {
            return static::$container->get('logger.default');
        });

        foreach (static::$container->get('config')['log'] as $channel => $config) {
            static::$container->share('logger.' . $channel, function () use ($channel, $config) {
                $logger = LoggerFactory::buildLogger($channel);
                $logger->pushHandler(
                    LoggerFactory::buildHandler($config['handler'], $config['config'])
                );

                return $logger;
            });
        }
    }

    protected function registerDatabaseConnection()
    {
        static::$container->share('db', function () {
            static::$container->get('log')->debug('Initializing database connection');
            return DriverManager::getConnection(static::$container->get('config')['database']);
        });
    }

    protected function registerEntityManager()
    {
        static::$container->share('em', function () {
            static::$container->get('log')->debug('Building entity manager');
            $cacheFactory = new CacheFactory();
            $config = ORM\Tools\Setup::createAnnotationMetadataConfiguration(
                array(__DIR__),
                static::$container->get('config')['devMode']
            );
            $config->setQueryCacheImpl(
                $cacheFactory->getDoctrineCacheBridge('doctrine_query')
            );
            $config->setMetadataCacheImpl(
                $cacheFactory->getDoctrineCacheBridge('doctrine_metadata')
            );
            $config->setResultCacheImpl(
                $cacheFactory->getDoctrineCacheBridge('doctrine_result')
            );

            return ORM\EntityManager::create(static::$container->get('db'), $config);
        });
    }

    protected function getCacheFactory(): CacheFactory
    {
        if (!$this->cacheFactory) {
            $this->cacheFactory = new CacheFactory();
        }

        return $this->cacheFactory;
    }

    /**
     * @return Container
     */
    static public function getContainer()
    {
        return static::$container;
    }

    /**
     * @return bool
     */
    public static function isInDevMode(): bool
    {
        return self::$inDevMode;
    }
}
