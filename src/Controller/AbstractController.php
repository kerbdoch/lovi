<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Controller;

use Aura\Router\Route;
use Doctrine\Common\Annotations\AnnotationReader;
use Kerbdoch\Lovi\Annotations\ActionCache;
use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Cache\CacheFactory;
use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Exception\Web\AbstractWebException;
use Kerbdoch\Lovi\Security\AccessControl;
use Kerbdoch\Lovi\Security\Authentication;
use Psr\Cache\CacheItemInterface;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Log\LoggerInterface;

abstract class AbstractController implements ControllerInterface
{
    /**
     * @var ServerRequestInterface
     */
    protected $request;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var Route
     */
    protected $route;

    /**
     * @var string
     */
    protected $action;

    /**
     * @var string
     */
    protected $defaultContentType = 'text/plain';

    abstract public function handleWebException(AbstractWebException $webException);

    abstract public function handleAccessViolation();

    public function __construct(ServerRequestInterface $request, ResponseInterface $response)
    {
        $this->request = $request;
        $this->response = $response;
    }

    public function call(Route $route): ResponseInterface
    {
        $this->route = $route;

        $this->action = explode('::', $this->route->handler)[1];
        if (!$this->action) {
            throw new ApplicationException("No action passed to controller", 1495122017176);
        } elseif (!method_exists($this, $this->action)) {
            throw new ApplicationException(
                sprintf(
                    'Action "%s" does not exist in "%s"',
                    $this->action,
                    static::class
                ),
                1495122979617
            );
        }

        if ($this->userHasAccess()) {
            $cacheInformation = $this->getCacheInformation();
            if ($cacheInformation && $cacheInformation->cacheable) {
                $cache = $this->getCache();
                $cacheItem = $cache->getItem(
                    $this->buildCacheIdentifier()
                );
                if ($cacheItem->isHit()) {
                    [$headers, $body] = $cacheItem->get();
                    foreach ($headers as $name => $values) {
                        foreach ($values as $value) {
                            $this->response = $this->response->withAddedHeader($name, $value);
                        }
                    }
                    $this->response->getBody()->write($body);
                } else {
                    $this->callAction();
                    $this->response->getBody()->rewind();
                    $cacheItem->set(
                        [
                            $this->response->getHeaders(),
                            $this->response->getBody()->getContents()
                        ]
                    );
                    $cacheItem->expiresAfter($cacheInformation->expires);
                    $cache->save($cacheItem);
                }
            } else {
                $this->callAction();
            }
        } else {
            $this->handleAccessViolation();
        }

        return $this->response;
    }

    public function callAction()
    {
        $this->response = $this->response->withHeader(
            'Content-Type',
            $this->defaultContentType
        );

        try {
            $this->{$this->action}();
        } catch (AbstractWebException $webException) {
            $this->handleWebException($webException);
        }
    }

    protected function userHasAccess()
    {
        if ($this->getAnnotation('Kerbdoch\Lovi\Annotations\NoAccessControl')) {
            return true;
        }

        $accessControlAnnotation = $this->getAnnotation('Kerbdoch\Lovi\Annotations\AccessControl');
        if (!$accessControlAnnotation) {
            /** @var LoggerInterface $logger */
            $logger = Application::getContainer()->get('log');
            $logger->warning(
                'The called action does not have an access control annotation. Access has been denied.',
                [__CLASS__, $this->action]
            );
            return false;
        }

        $accessControl = new AccessControl($accessControlAnnotation, $this->request);
        return $accessControl->userHasAccess();
    }

    protected function getAnnotation(string $annotationName)
    {
        $method = new \ReflectionMethod(static::class, $this->action);

        /** @var AnnotationReader $annotationReader */
        $annotationReader = Application::getContainer()->get('annotation_reader');
        return $annotationReader->getMethodAnnotation($method, $annotationName);
    }

    protected function getAnnotations()
    {
        $method = new \ReflectionMethod(static::class, $this->action);

        /** @var AnnotationReader $annotationReader */
        $annotationReader = Application::getContainer()->get('annotation_reader');
        return $annotationReader->getMethodAnnotations($method);
    }

    protected function getCacheInformation(): ?ActionCache
    {
        return $this->getAnnotation('Kerbdoch\Lovi\Annotations\ActionCache');
    }

    protected function buildCacheIdentifier()
    {
        return 'action.'
            . str_replace('\\', '_', __CLASS__)
            . '__' . $this->action
            . hash('sha256', serialize($this->request->getAttributes())
            );
    }

    protected function getCache(): CacheItemPoolInterface
    {
        $cacheFactory = new CacheFactory();
        return $cacheFactory->get('web');
    }
}
