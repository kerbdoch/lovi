<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Controller;

use Kerbdoch\Lovi\Annotations as Lovi;

class BundleController extends AbstractJsonController
{
    /**
     * @SWG\Post(
     *   path="/api/bundles",
     *   @SWG\Response(response="201", description="The newly created bundle")
     * )
     * @Lovi\ActionCache(cacheable=false)
     * @Lovi\AccessControl("LoggedIn")
     */
    public function createAction()
    {
    }

    /**
     * @SWG\Get(
     *   path="/api/bundles",
     *   @SWG\Response(response="200", description="The bundles the logged in user belongs to")
     * )
     * @Lovi\ActionCache(cacheable=false)
     * @Lovi\AccessControl("LoggedIn")
     */
    public function listAction()
    {
    }

    /**
     * @SWG\Get(
     *   path="/api/bundle/{id}",
     *   operationId="getBundleById",
     *   @SWG\Parameter(
     *     description="ID of bundle to return",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string",
     *     format="uuid"
     *   ),
     *   @SWG\Response(response="200", description="The requested bundle"),
     *   @SWG\Response(response="404", description="No bundle of the given ID exists or is accessible to the user")
     * )
     * @Lovi\ActionCache(cacheable=true)
     * @Lovi\AccessControl({"LoggedIn","BundleMembership"})
     */
    public function showAction()
    {
    }

    /**
     * @SWG\Patch(
     *   path="/api/bundle/{id}",
     *   operationId="updateBundleById",
     *   @SWG\Parameter(
     *     description="ID of bundle to update",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string",
     *     format="uuid"
     *   ),
     *   @SWG\Response(response="200", description="The updated bundle"),
     *   @SWG\Response(response="404", description="No bundle of the given ID exists or is visible to the user"),
     *   @SWG\Response(response="403", description="The given bundle is visible, but not editable for the user")
     * )
     * @Lovi\ActionCache(cacheable=false)
     * @Lovi\AccessControl({"LoggedIn","BundleMembership","BundleOwnership"})
     */
    public function updateAction()
    {
    }

    /**
     * @SWG\Delete(
     *   path="/api/bundle/{id}",
     *   operationId="deleteBundleById",
     *   @SWG\Parameter(
     *     description="ID of bundle to delete",
     *     in="path",
     *     name="id",
     *     required=true,
     *     type="string",
     *     format="uuid"
     *   ),
     *   @SWG\Response(response="200", description="The bundle does not exist (i.e. has been deleted)"),
     *   @SWG\Response(response="403", description="The bundle was not deleted because of lacking permissions")
     * )
     * @Lovi\ActionCache(cacheable=false)
     * @Lovi\AccessControl({"LoggedIn","BundleOwnership"})
     */
    public function deleteAction()
    {
    }
}
