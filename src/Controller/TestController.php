<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Controller;

use Kerbdoch\Lovi\Annotations as Lovi;

class TestController extends AbstractJsonController
{
    /**
     * @Lovi\ActionCache(cacheable=true, expires=60)
     */
    public function testAction()
    {
        $this->response->getBody()->write(
            json_encode([
                "foo" => "bar"
            ])
        );
    }
}
