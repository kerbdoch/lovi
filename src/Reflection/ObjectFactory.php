<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Reflection;

class ObjectFactory
{
    /**
     * @param string $class
     * @param array $arguments
     * @return object
     * @throws ReflectionException if a required argument is not set or the given class does not exist.
     */
    public static function new(string $class, array $arguments = [])
    {
        if (!class_exists($class)) {
            throw new ReflectionException(
                "Class \"$class\" could not be found or does not exist.",
                1495034607518
            );
        }

        $reflection = new \ReflectionClass($class);
        $arguments  = static::buildHandlerArguments($reflection, $arguments);

        $instance = $reflection->newInstanceArgs($arguments);

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $instance;
    }

    protected static function buildHandlerArguments(\ReflectionClass $reflectionClass, array $arguments): array
    {
        $constructorArgs = [];
        $reflectedArgs = $reflectionClass->getConstructor()->getParameters();

        foreach ($reflectedArgs as $reflectedArg) {
            if ($reflectedArg->isOptional()) {
                $constructorArgs[] = (array_key_exists($reflectedArg->getName(), $arguments))
                    ? $arguments[$reflectedArg->getName()]
                    : $reflectedArg->getDefaultValue();
            } else {
                if (!array_key_exists($reflectedArg->getName(), $arguments)) {
                    throw new ReflectionException(
                        "Required argument '{$reflectedArg->getName()}' for constructor of {$reflectionClass->getName()} is not set.",
                        1494342248568
                    );
                }

                $constructorArgs[] = $arguments[$reflectedArg->getName()];
            }
        }

        return $constructorArgs;
    }
}
