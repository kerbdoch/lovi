<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Web;

use Aura\Router\Route;
use Aura\Router\RouterContainer;
use Kerbdoch\Lovi\Application;
use Kerbdoch\Lovi\Controller\ControllerInterface;
use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Helper\RouteHelper;
use Psr\Http\Message\ResponseInterface;

class RequestHandler
{
    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var RouterContainer
     */
    protected $router;

    public function __construct(ResponseInterface $response = null)
    {
        $this->response = $response ?: Application::getContainer()->get('response');

        $this->buildRouter();
    }

    public function handle()
    {
        $matcher = $this->router->getMatcher();
        $route = $matcher->match(Application::getContainer()->get('request'));

        if ($route) {
            $this->handleMatchedRoute($route);
        } else {
            $this->handleFailedRoute($matcher->getFailedRoute());
        }
    }

    public function output()
    {
        foreach ($this->response->getHeaders() as $name => $values) {
            foreach ($values as $value) {
                header(sprintf('%s: %s', $name, $value), false);
            }
        }

        $ob = fopen('php://output', 'w');
        $this->response->getBody()->rewind();
        while ($body = $this->response->getBody()->read(2048)) {
            fwrite($ob, $body);
        }
        fclose($ob);
        $this->response->getBody()->close();
    }

    protected function buildRouter()
    {
        $this->router = new RouterContainer();

        $map = $this->router->getMap();
        RouteHelper::mapApiRoutes($map);
    }

    protected function handleMatchedRoute(Route $route)
    {
        $controller = explode("::", $route->handler)[0];

        if (!class_exists($controller)) {
            throw new ApplicationException(
                "Controller \"$controller\" does not exist.",
                1495190999374
            );
        } elseif (!in_array(ControllerInterface::class, class_implements($controller))) {
            throw new ApplicationException(
                "Class \"$controller\" does not implement ControllerInterface",
                1495190987676
            );
        }

        /** @var ControllerInterface $controller */
        $controller = new $controller(Application::getContainer()->get('request'), $this->response);

        $this->response = $controller->call($route);
    }

    protected function handleFailedRoute(Route $failedRoute)
    {
        switch ($failedRoute->failedRule) {
            case 'Aura\Router\Rule\Allows':
                // 405 Not Allowed

                break;
            case 'Aura\Router\Rule\Accepts':
                // 406 Not Acceptable
                break;
            default:
                // 404 Not Found
                break;
        }
    }
}
