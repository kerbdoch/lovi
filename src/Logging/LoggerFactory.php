<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Logging;

use Kerbdoch\Lovi\Exception\ApplicationException;
use Kerbdoch\Lovi\Reflection\ObjectFactory;
use Monolog\Handler\HandlerInterface;
use Monolog\Logger;

class LoggerFactory
{
    public static function buildLogger($channel): Logger
    {
        return new Logger($channel);
    }

    public static function buildHandler($handler, array $arguments = []): HandlerInterface
    {
        if (!class_exists($handler) || !in_array(HandlerInterface::class, class_implements($handler))) {
            throw new ApplicationException(
                "Class $handler does not exist or does not implement Monolog\\Handler\\HandlerInterface.",
                1494341712886
            );
        }

        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return ObjectFactory::new($handler, $arguments);
    }
}
