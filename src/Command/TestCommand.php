<?php

namespace Kerbdoch\Lovi\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TestCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('lovi:test')
            ->setDescription('A small test command')
            ->setHelp('This is just a test');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('Just a test.');
    }
}
