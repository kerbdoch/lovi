<?php
/**
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 */

namespace Kerbdoch\Lovi\Command;

use Kerbdoch\Lovi\Utility\PathUtility;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SwaggerCommand extends Command
{
    protected function configure()
    {
        $this->setName('lovi:swagger')
            ->setDescription('Generate Swagger information')
            ->setHelp('Generates all Swagger API information');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $swag = \Swagger\scan(__DIR__ . DIRECTORY_SEPARATOR . "..");
        file_put_contents(PathUtility::normalizePath(__DIR__ . "/../../web/swagger.json"), $swag);
    }
}
