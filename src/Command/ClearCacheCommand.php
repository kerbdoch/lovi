<?php
/**
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <noreply@felixrau.ch> wrote this file.  As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return.     - Felix Rauch
 * ----------------------------------------------------------------------------
 */

namespace Kerbdoch\Lovi\Command;

use Kerbdoch\Lovi\Cache\CacheFactory;
use Psr\Cache\CacheItemPoolInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ClearCacheCommand extends Command
{
    protected function configure()
    {
        $this->setName('lovi:clearcache')
            ->setDescription('Clears all caches')
            ->setHelp('Attempt to clear all registered caches')
            ->addOption(
                'dry',
                null,
                InputOption::VALUE_NONE,
                'If set, no caches will be flushed. Instead, any cache that would be flushed is written to STDOUT.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dry = $input->getOption('dry');
        $outputOptions = $dry ? OutputInterface::VERBOSITY_NORMAL : OutputInterface::VERBOSITY_VERBOSE;
        $cacheFactory = new CacheFactory();

        /**
         * @var string $name
         * @var CacheItemPoolInterface $cache
         */
        foreach ($cacheFactory->all() as [$name, $cache]) {
            if ($dry) {
                $output->writeln("Would clear cache \"$name\".", $outputOptions);
            } else {
                $output->write("Clearing cache \"$name\" ", false, $outputOptions);
                $success = $cache->clear();
                $output->write(($success ? 'succeeded.' : 'failed.'), true, $outputOptions);
            }
        }
    }
}
